<?php
    function download_sheet($sp_key) {
	$url = "https://spreadsheets.google.com/feeds/cells/{$sp_key}/public/basic?alt=json";
	$str = file_get_contents($url);
        $data = json_decode($str, true);
        $id_marker = "https://spreadsheets.google.com/feeds/cells/{$sp_key}/public/basic/";
        $entries   = $data["feed"]["entry"];

        $res = array();
        foreach($entries as $entry) {
           $content = $entry["content"];
           $ind = str_replace($id_marker."R", "", $entry["id"]['$t']);
           $ii  = explode("C", $ind);
           $res[$ii[0]-1][$ii[1]-1] = $entry["content"]['$t'];
        }

        return $res;
    }

$SP_key = "1CUTret1thdk3ItCBIDZUXd-iH3BDXPurYhmofreqQAY/6";
$rawdata = download_sheet($SP_key);
$data1 = json_encode($rawdata);

$SP_key = "1CUTret1thdk3ItCBIDZUXd-iH3BDXPurYhmofreqQAY/7";
$rawdata = download_sheet($SP_key);
$data2 = json_encode($rawdata);

$SP_key = "1CUTret1thdk3ItCBIDZUXd-iH3BDXPurYhmofreqQAY/8";
$rawdata = download_sheet($SP_key);
$data3 = json_encode($rawdata);

//$data = concat($data1,$data2,$data3);
$sdata = array_merge(json_decode($data1),json_decode($data2),json_decode($data3)); 
//echo $data ; exit;

//print_r($rows); exit;

//$sdata[] = json_decode($data1);
//$sdata[] = json_decode($data2);
//$sdata[] = json_decode($data3);

$json = json_encode($sdata);
?>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="js/jsapi"></script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/pivot.css">
        <script type="text/javascript" src="js/pivot.js"></script>
        <script type="text/javascript" src="js/gchart_renderers.js"></script>
        <style>
            body {font-family: Verdana;}
        </style>
    </head>
<?php 	

echo    '<body>
        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart", "charteditor"]});
            $(function(){
							  var derivers = $.pivotUtilities.derivers;
                var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.gchart_renderers);

                    $("#output").pivotUI('.$json.', 
										{
                        renderers: renderers,
												cols: ["YEAR","MONTH","WEEK"],
												rows: ["LOCATION"],
												vals: ["ADR"],
												aggregatorName: "Rata rata",
                        rendererName: "Line Chart"																		
                    });
										
                    $("#output2").pivotUI('.$json.', 
										{
                        renderers: renderers,
												cols: ["YEAR","MONTH","WEEK"],
												rows: ["LOCATION"],
												vals: ["REVENUE"],
												aggregatorName: "Jumlah",
                        rendererName: "Line Chart"																		
                    });
										

             });
				</script>				
				';
?>					         
        <div id="output" style="margin: 30px;"></div>
        <div id="output2" style="margin: 30px;"></div>
				<script type="text/javascript">
				var tableToExcel = (function() {
					var uri = 'data:application/vnd.ms-excel;base64,'
						, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
						, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
						, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
					return function(table, name) {
						if (!table.nodeType) table = document.getElementById(table)
						var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
						window.location.href = uri + base64(format(template, ctx))
					}
				})()
				</script>	
			<input type="button" onclick="tableToExcel('pvtTableOutput', 'Pivot Table Export')" value="Export to Excel">			
<?php 				
    echo '</body>
		</html>
';