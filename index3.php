<?php
require_once 'google/appengine/api/log/LogService.php';
require_once 'google/appengine/api/users/UserService.php';
use google\appengine\api\log\LogService;	
use google\appengine\api\users\UserService;
date_default_timezone_set('Asia/Bangkok');		

$user = UserService::getCurrentUser();
if (!$user) {
?>
<div class="module">
	<a href="
	<?php 
	echo UserService::createLoginURL('');
	?>
	">SILAHKAN LOG IN DISINI UNTUK MENGAKSES</a>
	</div>
<?php } 
		
date_default_timezone_set('Asia/Bangkok');


function getdbdata1($row) {	
	$test = json_encode($row);
	$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => "Content-Type: application/json\r\n",
			'content' => $test,
			'timeout' => 600
		)
	);
	$context  = stream_context_create($opts);
	$url = 'http://hrdsgbgroup.appspot.com/sheet3';
//	$url = 'http://127.0.0.1/sheet3.php';
	$output = file_get_contents($url, false, $context, -1, 4000000);
	//print_r($output);exit;
	return $output;
}

$row2 = array();
$data = getdbdata1($row2);
//print_r($data);exit;
	
$row2 = json_decode($data);
//print_r($row2);exit;

$json = json_encode($row2);

?>
<!DOCTYPE html>
<html>
    <head>
		<script type="text/javascript" src="js/jsapi"></script>
		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/pivot.css">
        <script type="text/javascript" src="js/pivot.js"></script>
        <script type="text/javascript" src="js/gchart_renderers.js"></script>
		<script type="text/javascript" src="js/ggrap.js"></script>
        <style>
            body {font-family: Verdana;}
        </style>
    </head>
<?php 	
echo    '<body>
        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart", "charteditor"]});
            $(function(){
							  var derivers = $.pivotUtilities.derivers;
                var renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.gchart_renderers);

                    $("#output").pivotUI('.$json.', 
										{
                        renderers: renderers,
                        rendererName: "Table"
                    });
             });
						 </script>				
						 ';
?>						         
        <div id="output" style="margin: 30px;"></div>
				<script type="text/javascript">
				var tableToExcel = (function() {
					var uri = 'data:application/vnd.ms-excel;base64,'
						, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
						, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
						, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
					return function(table, name) {
						if (!table.nodeType) table = document.getElementById(table)
						var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
						window.location.href = uri + base64(format(template, ctx))
					}
				})()
				</script>	
			<input type="button" onclick="tableToExcel('pvtTableOutput', 'Pivot Table Export')" value="Export to Excel">			
<?php 				
    echo '</body>
		</html>
';